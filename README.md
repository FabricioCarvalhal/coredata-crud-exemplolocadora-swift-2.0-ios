# Locadora de Filmes #

Este é uma clássica atividade normalmente usada em aulas de POO.
Desta vez feito em formato aplicativo, utilizando Swift 2.0, a linguagem de programação da Apple para os seus produtos.

### Por que estou compartilhando este projeto? ###

Durantes as aulas que tive o privilégio de participar do projeto Hackatruck, esse foi um dos primeiros exemplos que consegui fazer. E como conhecimento é poder, resolvi compartilhar aqui para que outras pessoas que estejam estudando a linguagem e estejam procurando referências e exemplos utilizando CoreData possam ver e aprender com ele.