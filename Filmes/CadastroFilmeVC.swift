//
//  CadastroFilmeVC.swift
//  Filmes
//
//  Created by Student on 4/22/16.
//  Copyright © 2016 Student. All rights reserved.
//

import UIKit

class CadastroFilmeVC: UIViewController {
    
    var filme: Filme!
    var generofilme: Generos!
    var generos = ["Terror","Comedia","Acao"]
    
    // MARK: Outlets
    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var ano: UITextField!
    @IBOutlet weak var diretor: UITextField!
    @IBOutlet weak var sinopse: UITextField!
    @IBOutlet weak var genero: UIPickerView!
    
    // MARK: Actions
    @IBAction func CadastrarBtn(sender: AnyObject) {
        filme = Filme()
        filme.nome = nome.text
        filme.ano = ano.text
        filme.diretor = diretor.text
        filme.sinopse = sinopse.text
        
        print(generos[genero.selectedRowInComponent(0)])
        generofilme = Generos()
        generofilme.nome = generos[genero.selectedRowInComponent(0)]
        
        filme.genero = generofilme
        FilmeDAO.inserir(filme)
        
        navigationController?.popViewControllerAnimated(true)
    }
    
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
            }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return generos.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return generos[row]
    }
    
    
}
