//
//  Filme.swift
//  Filmes
//
//  Created by Student on 4/25/16.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Filme: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    convenience init(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        // Informando qual tabela deve ser buscada no banco
        let entityDescription : NSEntityDescription = NSEntityDescription.entityForName("Filme", inManagedObjectContext: context)!
        
        self.init(entity: entityDescription, insertIntoManagedObjectContext: context)
    }
}
