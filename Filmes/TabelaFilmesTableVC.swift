//
//  TabelaFilmesTableVC.swift
//  Filmes
//
//  Created by Student on 4/22/16.
//  Copyright © 2016 Student. All rights reserved.
//

import UIKit

class TabelaFilmesTableVC: UITableViewController {
    var listaFilmes : [Filme]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //FilmeDAO.buscar()
        buscarDadosDoBanco()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        buscarDadosDoBanco()
        tableView.reloadData()
    }
    func buscarDadosDoBanco(){
        
        self.listaFilmes = [Filme]()
        self.listaFilmes = FilmeDAO.buscar()
    }
    
    // MARK: - Table view data source
  
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.listaFilmes!.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CelulaFilme", forIndexPath: indexPath) as! CelulaTabelaFilmes

        
       let c = listaFilmes![indexPath.row]
        cell.nome.text = c.nome
        cell.ano.text = c.genero?.nome
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if  segue.identifier == "toEditView" {
            if let destino = segue.destinationViewController as? EditarVC {
                destino.filme = listaFilmes![(tableView.indexPathForSelectedRow?.row)!]
            }
        }
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        FilmeDAO.excluir(listaFilmes![indexPath.row])
        tableView.reloadData()
    }
    
}
