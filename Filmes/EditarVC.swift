//
//  EditarVC.swift
//  Filmes
//
//  Created by Student on 4/22/16.
//  Copyright © 2016 Student. All rights reserved.
//

import UIKit

class EditarVC: UIViewController {
    @IBOutlet weak var nome: UITextField!
    @IBOutlet weak var ano: UITextField!
    @IBOutlet weak var diretor: UITextField!
    @IBOutlet weak var sinopse: UITextField!
    @IBOutlet weak var genero: UIPickerView!
    
    var generofilme: Generos!
    var generos = ["Terror","Comedia","Acao"]
    
    var filme : Filme?
    
    @IBAction func salvarEditBtn(sender: AnyObject) {
        filme?.nome = nome.text
        filme?.ano  = ano.text
        filme?.diretor = diretor.text
        filme?.sinopse = sinopse.text
        generofilme = Generos()
        generofilme?.nome = generos[genero.selectedRowInComponent(0)]
        filme?.genero = generofilme
        
        //comentado para testes
        FilmeDAO.alterar()
        navigationController?.popViewControllerAnimated(true)
    }
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        if filme != nil{
            self.nome.text = filme?.nome
            self.ano.text = filme?.ano
            self.diretor.text = filme?.diretor
            self.sinopse.text = filme?.sinopse
            self.genero.selectRow(generos.indexOf((filme?.genero!.nome)!)!, inComponent: 0, animated: true)
            //print(self.generos.indexOf("Comedia"))

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return generos.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return generos[row]
    }
    
}
