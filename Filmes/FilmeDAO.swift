//
//  FilmeDAO.swift
//  Filmes
//
//  Created by Student on 4/22/16.
//  Copyright © 2016 Student. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class FilmeDAO{
    
    static func inserir(filme: Filme) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        context.insertObject(filme)
        
        do {
            try context.save()
            print("foi")
        } catch let erro as NSError {
            print(erro)
        }
        
    }
    
    static func buscar()-> [Filme]{
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        var filmes: [Filme] = [Filme]()
        
  
        let request: NSFetchRequest = NSFetchRequest(entityName: "Filme")
        request.sortDescriptors = [NSSortDescriptor(key: "nome", ascending: true)]
        
        do {
            filmes = try context.executeFetchRequest(request) as! [Filme]
         //   print(filmes)
        } catch let erro as NSError {
            print(erro)
        }
        
        return filmes
    }
    
    
    static func alterar() {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        do {
            try context.save()
            
        } catch let erro as NSError {
            print(erro)
        }
    }
    
    static func excluir(filme: Filme) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        context.deleteObject(filme)
        
        do {
            try context.save()
            
        } catch let erro as NSError {
            print(erro)
        }
    }
}