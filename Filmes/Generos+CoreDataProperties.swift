//
//  Generos+CoreDataProperties.swift
//  Filmes
//
//  Created by Student on 4/25/16.
//  Copyright © 2016 Student. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Generos {

    @NSManaged var nome: String?
    @NSManaged var filme: NSSet?

}
